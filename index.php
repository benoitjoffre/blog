<?php
require_once('config/database.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">
    <title>Document</title>
</head>
<body>

<!--NAVBAR -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="#">Page</a>
      <a class="nav-item nav-link" href="#">Billet de blog</a>
      <a class="nav-item nav-link" href="login.php">Login</a>
    </div>
  </div>
</nav>

<!-- CARDS -->
<section class="container">

    <div class="card" style="width: 18rem;">
    <div class="card-img-top first"></div>
      <div class="card-body">
        <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minima, quas?        <a href="#">Lire plus...</a></p>
        <hr>
        <p>Auteur :</p>
        <p>Publié le :</p>
      </div>
    </div>


    <div class="card" style="width: 18rem;">
    <div class="card-img-top second"></div>
      <div class="card-body">
        <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minima, quas?        <a href="#">Lire plus...</a></p>
        <hr>
        <p>Auteur :</p>
        <p>Publié le :</p>
      </div>
    </div>

    <div class="card" style="width: 18rem;">
    <div class="card-img-top third"></div>
      <div class="card-body">
        <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minima, quas?        <a href="#">Lire plus...</a></p>
        <hr>
        <p>Auteur :</p>
        <p>Publié le :</p>
      </div>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>